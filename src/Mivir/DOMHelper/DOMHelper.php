<?php
namespace Mivir\DOMHelper;

class DOMHelper
{
    protected $domHandlers = [];

    /**
     * Register a handler or "document element".
     *
     * @param   string    $name     The name of the handler
     * @param   callable  $handler  A function that returns an instance of the handler
     *
     * @return  void
     */
    public function register($name, $handler)
    {
        $this->domHandlers[$name] = $handler;
    }

    /**
     * Registers the default handlers.
     */
    public function __construct()
    {
        $this->register('form', function($action = '') {
            $handler = new Handlers\Form();
            $handler = $handler->action($action);

            return $handler;
        });

        $this->register('input', function($name = '', $type = 'text') {
            $handler = new Handlers\Input();
            $handler = $handler->name($name);
            $handler = $handler->type($type);

            return $handler;
        });

        $this->register('select', function($name = '') {
            $handler = new Handlers\Select();
            $handler = $handler->name($name);

            return $handler;
        });

        $this->register('textarea', function($name = '') {
            $handler = new Handlers\Textarea();
            $handler = $handler->name($name);

            return $handler;
        });
    }

    /**
     * Tries to match the called method name to a handler name and if a match is found,
     * creates a handler with that name, optionally passing the given arguments to the handler's creation function.
     *
     * @param   string  $method  Method name
     * @param   array   $args    Method arguments
     *
     * @return  object  The created handler
     */
    public function __call($method, $args)
    {
        if (array_key_exists($method, $this->domHandlers)) {
            $methodToCall = $this->domHandlers[$method];

            switch (count($args)) {
                case 0:
                    return $methodToCall();
                    break;
                case 1:
                    return $methodToCall($args[0]);
                    break;
                case 2:
                    return $methodToCall($args[0], $args[1]);
                    break;
                case 3:
                    return $methodToCall($args[0], $args[1], $args[2]);
                    break;
                case 4:
                    return $methodToCall($args[0], $args[1], $args[2], $args[3]);
                    break;
                case 5:
                    return $methodToCall($args[0], $args[1], $args[2], $args[3], $args[4]);
                    break;
                default:
                    call_user_func_array($methodToCall, $args);
                    break;
            }
        } else {
            throw new \Exception("Unknown DOMHelper handler: " . htmlentities($method, ENT_QUOTES, 'UTF-8'));
        }
    }
}