<?php
namespace Mivir\DOMHelper\Handlers;

class Input extends Base
{
    protected $attributes = [
        'type' => 'text'
    ];

    public function renderHTML()
    {
        $domDocument = new \DOMDocument('1.0', 'utf-8');
        $element = $domDocument->createElement('input');

        foreach ($this->attributes as $attribute => $value) {
            $element->setAttribute($attribute, $value);
        }

        $domDocument->appendChild($element);
        return $domDocument->saveHTML();
    }
}