<?php
namespace Mivir\DOMHelper\Handlers;

class Textarea extends Base
{
    protected $attributes = [
        'type' => 'text'
    ];

    public function renderHTML()
    {
        $domDocument = new \DOMDocument('1.0', 'utf-8');

        if (array_key_exists('value', $this->attributes)) {
            $element = $domDocument->createElement('textarea', $this->attributes['value']);
        } else {
            $element = $domDocument->createElement('textarea');
        }

        foreach ($this->attributes as $attribute => $value) {
            $element->setAttribute($attribute, $value);
        }

        $domDocument->appendChild($element);
        return $domDocument->saveHTML();
    }
}