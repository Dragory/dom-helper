<?php
namespace Mivir\DOMHelper\Handlers;

class Select extends Base
{
    protected $options = [];

    protected $defaultOption = null;

    public function options($options)
    {
        $this->options = $options;
        return $this;
    }

    public function value($value)
    {
        $this->defaultOption = $value;
        return $this;
    }

    public function renderHTML()
    {
        $domDocument = new \DOMDocument('1.0', 'utf-8');
        $element = $domDocument->createElement('select');

        foreach ($this->attributes as $attribute => $value) {
            $element->setAttribute($attribute, $value);
        }

        foreach ($this->options as $value => $text) {
            if (is_array($text)) {
                $optGroup = $domDocument->createElement('optgroup');
                $optGroup->setAttribute('label', $value);

                foreach ($text as $subValue => $subText) {
                    $option = $domDocument->createElement('option', $subText);
                    $option->setAttribute('value', $subValue);

                    if ($subValue == $this->defaultOption) {
                        $option->setAttribute('selected', 'selected');
                    }

                    $optGroup->appendChild($option);
                }

                $element->appendChild($optGroup);
            } else {
                $option = $domDocument->createElement('option', $text);
                $option->setAttribute('value', $value);

                if ($value == $this->defaultOption) {
                    $option->setAttribute('selected', 'selected');
                }

                $element->appendChild($option);
            }
        }

        $domDocument->appendChild($element);
        return $domDocument->saveHTML();
    }
}