<?php
namespace Mivir\DOMHelper\Handlers;

class Form extends Base
{
    // Default attributes
    protected $attributes = [
        'action' => '',
        'method' => 'post'
    ];

    // Are we outputting the form's end?
    protected $isTheEnd = false;

    // Charset
    protected $charset = 'utf-8';

    // CSRF token
    protected $hasCsrfToken = false;

    protected $csrfTokenInputName = '_token';

    protected $csrfToken = '';

    public function end()
    {
        // https://play.spotify.com/track/5UgT7w6zVZjP3oyawMzbiK
        $this->isTheEnd = true;
        return $this;
    }

    public function charset($charset)
    {
        $this->charset = $charset;
        return $this;
    }

    public function csrf($inputNameOrToken, $token = null)
    {
        $this->hasCsrfToken = true;

        // If we only have one attribute, assume the user
        // wants to use the default CSRF token input name.
        if ($token === null) {
            $this->csrfToken = $inputNameOrToken;
        }
        
        // If we have two attributes, assume the first one
        // is the desired name for the CSRF token  input.
        else {
            $this->csrfTokenInputName = $inputNameOrToken;
            $this->csrfToken = $token;
        }

        return $this;
    }

    public function renderHTML()
    {
        if ($this->isTheEnd) {
            return "</form>";
        }

        // Form an attribute string from our attributes
        $attributes = [];
        foreach ($this->attributes as $attribute => $value) {
            $attributes[] = $attribute . '="' . str_replace('"', '\\"', $value) . '"';
        }

        // Add our charset attribute
        $attributes[] = 'accept-charset="' . $this->charset . '"';

        // Start the form with the attribute strings from above
        $formHTML = "<form " . implode(' ', $attributes) . ">";

        // With UTF-8 we should enforce the charset with a hidden input
        if ($this->charset === 'utf-8') {
            $formHTML .= '<input type="hidden" name="utf-8" value="&#9731;">';
        }

        // Do we have a CSRF token? Add an element for that.
        if ($this->hasCsrfToken === true) {
            $formHTML .= '<input type="hidden" name="' . $this->csrfTokenInputName . '" value="' . $this->csrfToken . '">';
        }

        return $formHTML;
    }
}