<?php
namespace Mivir\DOMHelper\Handlers;

abstract class Base
{
    protected $attributes = [];

    public function __toString()
    {
        return $this->renderHTML();
    }

    abstract public function renderHTML();

    public function __call($method, $args)
    {
        if (count($args) === 0) {
            throw new \RuntimeException("Attribute methods require an argument!");
        }

        $attributeName = preg_replace_callback('/([A-Z])/s', function($matches) {
            return '-' . strtolower($matches[1]);
        }, $method);

        $this->attributes[$attributeName] = $args[0];

        return $this;
    }
}